/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lt.fx.common;

import javafx.scene.control.Control;

/**
 *
 * @author Robert.Orlowski
 */
@SuppressWarnings("serial")
public class LtValidateException extends RuntimeException{

    private Control control;
    
    public LtValidateException(String message, Control control) {
        super(message);
        
        this.control = control;        
    }

    public LtValidateException(String message) {
        super(message);
    }
    
    public LtValidateException(String message, Throwable cause) {
        super(message, cause);
    }

    public Control getControl() {
        return control;
    }    
}
