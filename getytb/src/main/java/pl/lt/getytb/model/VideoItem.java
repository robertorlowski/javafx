/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lt.getytb.model;

/**
 *
 * @author Robert.Orlowski
 */
public class VideoItem {
    
    private String url = "";
    private String title = "";
    private String folder = "";
    private Boolean error = false;

    public VideoItem(String url, String folder) {
        this.url = url;
        this.folder = folder;
    }
        
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
       
}
