/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lt.getytb;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.google.api.services.youtube.model.PlaylistItem;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import pl.lt.fx.common.GuiUtilsFX;
import pl.lt.fx.common.LtValidateException;
import pl.lt.fx.common.ValidateUtils;
import pl.lt.fx.common.google.UploadsYouTube;
import pl.lt.getytb.model.VideoItem;

/**
 * FXML Controller class
 *
 * @author Robert.Orlowski
 */
public class MainViewController implements Initializable {

    @FXML
    private Button pbDownload;
    @FXML
    private ProgressBar pbMain;
    @FXML
    private Label lblAddress;
    @FXML
    private TextField dfAddress;
    @FXML
    private Label lblTitleVideo;
    @FXML
    private Label lblDownloadFolder;
    @FXML
    private TextField dfFolder;
    @FXML
    private Button pbChooseFolder;
    @FXML
    private Button pbAdd;
    @FXML
    private Button pbDelete;
    @SuppressWarnings("rawtypes")
    @FXML
    private ListView listViewMain;

    private Stage mainStage;
    private final ObservableList<VideoItem> videoList;
    private final ExecutorService executorService = Executors.newFixedThreadPool(5);

    private Boolean executeTask = false;
    private YouTubeDownload.DownloadListener downloadListener;

    private ImageView imageViewFolder;
    private ImageView imageViewDowload;
    private ImageView imageViewCancel;
    private ImageView imageViewAdd;
    private ImageView imageViewDelete;

    public MainViewController() {
        this.videoList = FXCollections.observableArrayList();
    }

    public void setStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
    

    public void cancelDownload() {
        executeTask = false;
        executorService.shutdown();        
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        imageViewFolder = new ImageView("/images/folder.png");      
        imageViewDowload = new ImageView("/images/download.png");
        imageViewCancel = new ImageView("/images/cancel.png");

        imageViewAdd = new ImageView("/images/plus.png");
        imageViewDelete = new ImageView("/images/trash.png");

        lblTitleVideo.setText("");

        pbChooseFolder.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        pbChooseFolder.setGraphic(imageViewFolder);
        pbChooseFolder.setTooltip(new Tooltip("Set destibation folder."));

        pbDownload.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        pbDownload.setGraphic(imageViewDowload);
        pbDownload.setTooltip(new Tooltip("Start/Stop download."));

        pbAdd.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        pbAdd.setGraphic(imageViewAdd);
        pbAdd.setTooltip(new Tooltip("Add to list"));
        
        pbDelete.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        pbDelete.setGraphic(imageViewDelete);
        pbDelete.setTooltip(new Tooltip("Remove from list."));

        //dfAddress.setText("https://www.youtube.com/watch?v=j7zjhNblVO4");
        dfAddress.setText("");
        dfAddress.setOnKeyPressed((KeyEvent ke) -> {
            if (ke.getCode().equals(KeyCode.V) && ke.isControlDown()) {
                Platform.runLater(() -> {
                    pbAdd.fire();
                });
            }
        });
        dfFolder.setText(Paths.get("").toAbsolutePath().toString() + "\\");

        listViewMain.setItems(videoList);
        listViewMain.setCellFactory(new Callback<ListView<VideoItem>, ListCell<VideoItem>>() {

            @Override
            public ListCell<VideoItem> call(ListView<VideoItem> param) {
                ListCell<VideoItem> cell = new ListCell<VideoItem>() {

                    
                    @Override
                    protected void updateItem(VideoItem t, boolean bln) {
                        super.updateItem(t, bln);
                        if (bln) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(StringUtils.isEmpty(t.getTitle()) ? t.getUrl() : t.getTitle());
                        
                        }
                        
                    }
                };
                return cell;
            }
        });

        downloadListener = new YouTubeDownload.DownloadListener() {

            @Override
            public void downloadStart(final VideoItem videoItem) {
                Platform.runLater(() -> {
                    pbMain.setProgress(0);
                    MainApp.showAppMessege(getMovieTitle(videoItem));
                    lblTitleVideo.setText(getMovieTitle(videoItem));
                    pbDownload.getTooltip().setText("Downloading - "  + getMovieTitle(videoItem));
                });
            }

            @Override
            public Boolean downloadProgress(final VideoItem videoItem, double progress) {
                Platform.runLater(() -> {           
                    
                    if ( Boolean.FALSE.equals(videoItem.getError()) ) {
                        pbMain.setProgress(progress);
                        lblTitleVideo.setText(getMovieTitle(videoItem));
                        MainApp.setAppToolTipMessage(getMovieTitle(videoItem) +" - " + Math.round(progress*100) +"%");
                    } else {        
                        
                        if ( executeTask )  {
                            pbDownload.fire(); //STOP
                        }
                        
                        for ( Object vv :  listViewMain.getItems() ) {
                            if (vv instanceof VideoItem && videoItem.getUrl().equals(((VideoItem)vv).getUrl())) {
                                videoItem.setTitle("ERROR - " + getMovieTitle(videoItem) );
                                GuiUtilsFX.updateListView(listViewMain, videoItem, listViewMain.getItems().indexOf(vv));
                            }
                        }
                        
                        //move next 
                        listViewMain.getSelectionModel().selectNext();
                        final VideoItem vi = (VideoItem) listViewMain.getSelectionModel().getSelectedItem();
                        
                        // 
                        if ( !videoItem.getUrl().equals( vi.getUrl() )  ) {
                            pbDownload.fire(); //START
                        }
                    }
                });
                return Boolean.TRUE.equals(executeTask) && progress >=0;
            }

            @Override
            public void downloadFinished(final VideoItem videoItem, Boolean terminate) {
                Platform.runLater(() -> {
                    pbDownload.getTooltip().setText("Start/Stop download.");
                    MainApp.setAppToolTipMessage(MainApp.TITLE_APP);
                    
                    if (terminate) {
                        pbMain.setProgress(0);
                        lblTitleVideo.setText("");
                        
                    } else {
                        executeTask = false;
                        pbDownload.setGraphic(imageViewDowload);
                        
                        pbMain.setProgress(0);
                        lblTitleVideo.setText("");

                        videoList.remove(videoItem);
                        pbDownload.fire();
                    }
                });
            }

            @Override
            public void extracted(VideoItem videoItem) {
                Platform.runLater(() -> {
                    for (int iii = 0; iii < videoList.size(); iii++) {
                        if (videoList.get(iii).getUrl().equals(videoItem.getUrl())) {
                            GuiUtilsFX.updateListView(listViewMain, videoItem, iii);
                        }
                    }
                });
            }
        };
    }

    protected String getMovieTitle(VideoItem videoItem) {
        if ( videoItem == null ) {
            return "undefined";
        }
        
        return videoItem.getTitle() == null ? videoItem.getUrl() : videoItem.getTitle();
    }

    @FXML
    public void onClickDowloadVideo(ActionEvent event) {
        if (executeTask) {
            executeTask = false;
            pbDownload.setGraphic(imageViewDowload);
            
            return;
        }

        if (videoList.isEmpty()) {
            return;
        }

        if (StringUtils.isEmpty(dfFolder.getText())) {
            throw new LtValidateException("Invalid download folder.");
        }

        File file = new File(dfFolder.getText());
        if (!file.exists()) {
            try {
                FileUtils.forceMkdir(file);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        if (listViewMain.getSelectionModel().getSelectedItem() == null && !videoList.isEmpty() || event == null) {
            listViewMain.getSelectionModel().selectFirst();
        }

        if (listViewMain.getSelectionModel().getSelectedItem() == null) {
            return;
        }

        final VideoItem vi = (VideoItem) listViewMain.getSelectionModel().getSelectedItem();

        pbDownload.setGraphic(imageViewCancel);       
        executeTask =true;

        executorService.submit(() -> {
            YouTubeDownload e = new YouTubeDownload(downloadListener);
            e.download(vi);
        });
    }

    @FXML
    public void onClickChooseFolder(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory( new File( dfFolder.getText().trim() ) );
        File selectedDirectory = directoryChooser.showDialog(mainStage);

        if (selectedDirectory != null) {
            dfFolder.setText(selectedDirectory.getAbsolutePath());
        }
    }

    @FXML
    public void onClickAdd(ActionEvent event) {

        if (StringUtils.isEmpty(dfAddress.getText())) {
            return;
        }

        if (!ValidateUtils.validateUrl(dfAddress.getText())) {
            throw new LtValidateException("Invalid YouTube video address.", dfAddress);
        }

        if (dfAddress.getText().startsWith("https://www.youtube.com/playlist?list=")) {

            String[] playList = dfAddress.getText().trim().split("=");
            if (playList.length == 1) {
                return;
            }

            UploadsYouTube uyt = new UploadsYouTube();
            List<PlaylistItem> lll = uyt.getVideo4PlayList(playList[1]);
            lll.forEach((PlaylistItem t) -> {
                addItem("https://www.youtube.com/watch?v=" + t.getSnippet().getResourceId().getVideoId(), dfFolder.getText());
            });

        } else {
            addItem(dfAddress.getText(), dfFolder.getText());
            if (!executeTask && listViewMain.getItems().size() == 1 ) {
                onClickDowloadVideo(null);
            }
        }

        
        
        dfAddress.setText("");
        

    }

    private void addItem(String url, String folder) {
        for (int iii = 0; iii < videoList.size(); iii++) {
            if (videoList.get(iii).getUrl().equals(url)) {
                return;
            }            
        }
        
        final VideoItem item = new VideoItem(url.trim(), folder.trim());
        
        executorService.submit(() -> {
            YouTubeDownload e = new YouTubeDownload(downloadListener);
            e.prepare(item);
        });
        
        
        videoList.add(item);
    }

    @FXML
    public void onClickDelete(ActionEvent event) {
        if (videoList.isEmpty()) {
            return;
        }

        if (listViewMain.getSelectionModel().getSelectedItem() == null) {
            listViewMain.getSelectionModel().selectFirst();
        }

        if (listViewMain.getSelectionModel().getSelectedIndex() >= 0) {
            videoList.remove(listViewMain.getSelectionModel().getSelectedIndex());
        }
    }

}
