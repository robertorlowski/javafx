/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lt.getytb;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.github.axet.vget.VGet;
import com.github.axet.vget.info.VGetParser;
import com.github.axet.vget.info.VideoFileInfo;
import com.github.axet.vget.info.VideoInfo;
import com.github.axet.vget.vhs.VimeoInfo;
import com.github.axet.vget.vhs.YouTubeInfo;
import com.github.axet.vget.vhs.YouTubeInfo.StreamCombined;
import com.github.axet.vget.vhs.YouTubeInfo.StreamInfo;
import com.github.axet.vget.vhs.YouTubeMPGParser;
import com.github.axet.wget.info.DownloadInfo.Part;
import com.github.axet.wget.info.DownloadInfo.Part.States;
import com.github.axet.wget.info.ex.DownloadInterruptedError;

import pl.lt.getytb.model.VideoItem;

public class YouTubeDownload {
    
    private VideoInfo videoinfo;
    private long last;
    private int errorCount = 0;
    private DownloadListener downloadListener;

    public interface DownloadListener {
        void downloadStart(final VideoItem videoItem);
        Boolean downloadProgress(final VideoItem videoItem, final double progress);
        void downloadFinished(final VideoItem videoItem, Boolean terminate);
        void extracted(final VideoItem videoItem);
    }

    public YouTubeDownload(DownloadListener downloadListener) {
        this.downloadListener = downloadListener;
    }

    public void download(VideoItem videoItem) {
        try {
            run(videoItem, true);
        } catch (DownloadInterruptedError | InterruptedException | MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void prepare(VideoItem videoItem) {
        try {
            run(videoItem, false);
        } catch (DownloadInterruptedError | InterruptedException | MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void run(VideoItem videoItem, Boolean download) throws DownloadInterruptedError, InterruptedException, MalformedURLException {
        AtomicBoolean stop = new AtomicBoolean(false);
        
        Runnable notify = () -> {
            
            List<VideoFileInfo> list = videoinfo.getInfo();          

            // notify app or save download state
            // you can extract information from DownloadInfo info;
            switch (videoinfo.getState()) {
                case EXTRACTING_DONE:
                    if (videoinfo instanceof YouTubeInfo) {
                        YouTubeInfo i = (YouTubeInfo) videoinfo;
                        
                        videoItem.setTitle(i.getTitle());                        
                        downloadListener.extracted(videoItem);
                        System.out.println(videoinfo.getState() + " " + i.getVideoQuality());
                        
                    } else if (videoinfo instanceof VimeoInfo) {
                        VimeoInfo i = (VimeoInfo) videoinfo;
                        
                        videoItem.setTitle(videoinfo.getTitle());
                        downloadListener.extracted(videoItem);

                        System.out.println(videoinfo.getState() + " " + i.getVideoQuality());
                        
                    } else {
                        System.out.println("downloading unknown quality");
                    }

                    break;

                case ERROR:
                case RETRYING:
                    errorCount++;
                    System.out.println(videoinfo.getState() + " ERROR COUNT: " + errorCount + " - " + videoItem.getTitle());
                    
                    if (errorCount > 100 ) {
                        videoItem.setError(true);
                        if (!downloadListener.downloadProgress(videoItem, -1)) {
                            stop.set(true);
                            downloadListener.downloadFinished(videoItem, true);
                        }
                    }
                    break;
                    
                case DOWNLOADING:
                    
                    long now = System.currentTimeMillis();
                    if (now - 1000 > last) {
                        last = now;

                        String parts = "";

                        for (VideoFileInfo dinfo : list) {
                            List<Part> pp = dinfo.getParts();
                            if (pp != null) {
                                // multipart download
                                for (Part p : pp) {                                    
                                    if (p.getState().equals(States.DOWNLOADING)) {
                                        parts += String.format("part#%d(%.2f) ", p.getNumber(), p.getCount() / (float) p.getLength());
                                    }
                                }
                            }
                            
                            System.out.println(String.format("file:%d - %s %.2f %s ", list.indexOf(dinfo),
                                    videoinfo.getState(), dinfo.getCount() / (float) dinfo.getLength(), parts));
                                                      
                            if (!downloadListener.downloadProgress(videoItem,  dinfo.getCount() / (double) dinfo.getLength())) {
                                stop.set(true);
                                downloadListener.downloadFinished(videoItem, true);

                            }
                        }
                    }                    
                    break;
                                   
                case DONE:
                    downloadListener.downloadFinished(videoItem, stop.get());
                    System.out.println(videoinfo.getState() + " -" + videoItem.getTitle());                    
                    break;
                    
                default:
                    break;
            }
        };

        URL web = new URL(videoItem.getUrl());

        // [OPTIONAL] limit maximum quality, or do not call this function if
        // you wish maximum quality available.
        //
        // if youtube does not have video with requested quality, program
        // will raise en exception.
        VGetParser user = null;
        
        // create proper html parser depends on url
        //user = VGet.parser(web);
        
        // download maximum video quality from youtube
        // user = new YouTubeQParser(YoutubeQuality.p480);
        // download mp4 format only, fail if non exist
        user = new YouTubeMPGParser() {

            @Override
            public void filter(List<VideoDownload> sNextVideoURL, String itag, URL url) {
                Integer i = Integer.decode(itag);
                StreamInfo vd = itagMap.get(i);
                
                if (  vd instanceof StreamCombined  ) {
                    super.filter(sNextVideoURL, itag, url);
                }
                
            }
            
        };     

        // create proper videoinfo to keep specific video information        
        videoinfo = user.info(web);    

        VGet v = new VGet(videoinfo, new File( videoItem.getFolder() ));
        // [OPTIONAL] call v.extract() only if you d like to get video title
        // or download url link
        // before start download. or just skip it.
        v.extract(user, stop, notify);

        if (download) {
            downloadListener.downloadStart(videoItem);
            v.download(user, stop, notify);            
        }        
    }
}
