/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lt.fx.common;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.control.ListView;

/**
 *
 * @author Robert.Orlowski
 */
public class GuiUtilsFX {
    
     public static <T> void updateListView(ListView<T> listView, T newValue, int i) {
        EventType<? extends ListView.EditEvent<T>> type = ListView.editCommitEvent();
        Event event = new ListView.EditEvent<>(listView, type, newValue, i);
        listView.fireEvent(event);
    }
}
