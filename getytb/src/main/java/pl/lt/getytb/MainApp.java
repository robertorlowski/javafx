package pl.lt.getytb;


import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.apache.commons.lang.exception.ExceptionUtils;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pl.lt.fx.common.FXAlert;
import pl.lt.fx.common.LtValidateException;

public class MainApp extends Application {

    public static final String TITLE_APP = "YouTube downloader";
    private static TrayIcon trayIcon;
    private static SystemTray tray;
    
    private final FXMLLoader loader = new FXMLLoader();
    private Scene mainScene;
    private Stage primaryStage;
    
    public static void main(String[] args) throws Exception {
        launch(args);
    }

    private void startup() {

        try {
            tray = SystemTray.getSystemTray();
            java.awt.Image trayImage = Toolkit.getDefaultToolkit().getImage(MainApp.class.getResource("/images/download.png"));
            final PopupMenu popup = new PopupMenu();

            registerShowView(popup);
            
            registerExitApp(popup);

            trayIcon = new TrayIcon(trayImage, TITLE_APP, popup);
            tray.add(trayIcon);

            showAppMessege(TITLE_APP);
            
        } catch (Exception e) {            
        }

    }
    
    private void registerShowView(PopupMenu popup) {
        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Platform.runLater(new Runnable() {                    
                    @Override
                    public void run() {
                        primaryStage.show();        
                    }
                });
                                
            }
        };

        addMenuItemToPopup(popup, "Show - " + TITLE_APP , listener);          
    }

    private void registerExitApp(PopupMenu popup) {
        ActionListener exitListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                shutdown();
            }
        };

        popup.addSeparator();   
        addMenuItemToPopup(popup, "Close - " + TITLE_APP, exitListener);       
    }
    
    public void shutdown() {     
        if ( tray != null && trayIcon != null ) {
            tray.remove(trayIcon);
        }              
        Platform.exit();
        System.exit(0);
    }
   

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        
        Platform.setImplicitExit(false);
      
        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
            Throwable ex = ExceptionUtils.getRootCause(throwable);

            if (ex != null && ex instanceof LtValidateException) {
                FXAlert.showWarningDialog(ex.getMessage());

                Control ccc = ((LtValidateException) ex).getControl();
                if (ccc != null) {
                    ccc.requestFocus();
                }

            } else {
                FXAlert.showExceptionDialog(throwable);
            }
        });
        
        
        
        String fxmlFile = "/fxml/mainView.fxml";
        Parent rootNode = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));

        mainScene = new Scene(rootNode);
        mainScene.getStylesheets().add("styles/stylesheet.css");
        
        
        primaryStage.setTitle(TITLE_APP);
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("/images/download.png"));
        
        ((MainViewController) loader.getController()).setStage(primaryStage);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {            
            @Override
            public void handle(WindowEvent event) {
                if ( WindowEvent.WINDOW_CLOSE_REQUEST.equals(event.getEventType())) {
                    primaryStage.hide();
                    event.consume();
                }
            }
        });
        
        primaryStage.show();        
        startup();
    }

    @Override
    public void stop() throws Exception {
        ((MainViewController) loader.getController()).cancelDownload();
        super.stop();
        shutdown();
    }
    
    public static void showAppMessege(String message) {
        if ( tray == null || trayIcon == null ) {
            return;
        }     
        
        trayIcon.displayMessage(TITLE_APP, message, MessageType.INFO);
        trayIcon.setToolTip(message);
    }
    
    public static void setAppToolTipMessage(String message) {
        if ( tray == null || trayIcon == null ) {
            return;
        }     
        trayIcon.setToolTip(message);
    }
    
    private static MenuItem addMenuItemToPopup(PopupMenu popup, String label, ActionListener actionListener) {
        MenuItem menu = new MenuItem(label);
        menu.addActionListener(actionListener);
        popup.add(menu);
        
        return menu;
    }
}
